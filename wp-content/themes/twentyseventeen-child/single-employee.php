<?php
get_header();
the_post();
?>

<div class="wrap employee">
	<div class="employee_row">
		<div class="employee__content">
			<?php the_content();?>
		</div>
		<div class="employee__details">
			<div class="employee__column">
				<p><span class="employee__item">Name: </span><?php echo get_field('name');?></p>
				<p><span class="employee__item">Email: </span><?php echo get_field('email');?></p>
				<p><span class="employee__item">WWW: </span><a href="<?php echo get_field('www');?>"><?php echo get_field('www');?></a></p>
                <p><span class="employee__item"><?php echo get_field('sc_label1');?>: </span><a href="<?php echo get_field('sc_url1');?>"><?php echo get_field('sc_url1');?></a></p>
				<p><span class="employee__item"><?php echo get_field('sc_label2');?>: </span><a href="<?php echo get_field('sc_url2');?>"><?php echo get_field('sc_url2');?></a></p>
				<p><span class="employee__item"><?php echo get_field('sc_label3');?>: </span><a href="<?php echo get_field('sc_url3');?>"><?php echo get_field('sc_url3');?></a></p>
			</div>
			<div class="employee__column">
				<?php
				$image = get_field('image');
				if($image) {
					echo wp_get_attachment_image( $image, 'medium' );
				}
				?>
			</div>
		</div>
		<div class="employee_row">
			<div class="employee__related">
				<?php $related_posts = get_field('related_posts');

					foreach($related_posts as $p) {
						echo '<div class="post">';
							echo '<h2>'.$p->post_title.'</h2>';
							if(get_the_post_thumbnail($p->ID)) {
								echo '<div>' . get_the_post_thumbnail( $p->ID, 'thumb' ) . '</div>';
							}
							echo '<p>'.$p->post_name.'</p>';
							echo '<p><a href="'.get_permalink($p->ID).'">'.__('Read more', 'twentyseventeenchild').'</a></p>';
						echo '</div>';
					}
				?>
			</div>
		</div>
	</div>
</div>

<?php
get_footer();
?>