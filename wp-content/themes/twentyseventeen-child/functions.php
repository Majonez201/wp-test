<?php
//register ACF fields
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_data-adresses',
		'title' => 'Data adresses',
		'fields' => array (
			array (
				'key' => 'field_5a1d6c38a6ca3',
				'label' => 'logo',
				'name' => 'logo',
				'type' => 'image',
				'save_format' => 'id',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_5a1d6c6ea6ca4',
				'label' => 'Address',
				'name' => 'address',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_5a1d6c7fa6ca5',
				'label' => 'Map',
				'name' => 'map',
				'type' => 'google_map',
				'center_lat' => '',
				'center_lng' => '',
				'zoom' => '',
				'height' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'contact.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

function my_acf_google_map_api( $api ) {
	$api['key'] = 'AIzaSyABLVo-tUAbTWlhPwETtbHycjTLjWHcgEI';
	return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

function googleMapScript() {
	?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyABLVo-tUAbTWlhPwETtbHycjTLjWHcgEI"></script>
	<?php
}
add_action( 'wp_footer', 'googleMapScript');

register_post_type( 'employee',
	array(
		'labels'      => array(
			'name'          => __( 'Employee',  'twentyseventeenchild'),
			'singular_name' => __( 'Employee', 'twentyseventeenchild' )
		),
		'public'      => true,
		'has_archive' => true,
		'supports' => array(
			'title',
			'editor',
			'thumbnail'
		)
	)
);

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_data',
		'title' => 'Data',
		'fields' => array (
			array (
				'key' => 'field_5a1d85e5b2622',
				'label' => 'Image',
				'name' => 'image',
				'type' => 'image',
				'save_format' => 'id',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_5a1d85f7b2623',
				'label' => 'Name',
				'name' => 'name',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a1d8601b2624',
				'label' => 'Email',
				'name' => 'email',
				'type' => 'email',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
			),
			array (
				'key' => 'field_5a1d8610b2625',
				'label' => 'WWW',
				'name' => 'www',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a1d861cb2626',
				'label' => 'SocialMedia(label)',
				'name' => 'sc_label1',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a1d8839b2629',
				'label' => 'SocialMedia(URL)',
				'name' => 'sc_url1',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a1d877eb2627',
				'label' => 'ScocialMedia(label)',
				'name' => 'sc_label2',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a1d8850b262b',
				'label' => 'SocialMedia(URL)',
				'name' => 'sc_url2',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a1d87b1b2628',
				'label' => 'SocialMedia(label)',
				'name' => 'sc_label3',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a1d8850b262a',
				'label' => 'SocialMedia(URL)',
				'name' => 'sc_url3',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a1d8897b262c',
				'label' => 'Related Posts',
				'name' => 'related_posts',
				'type' => 'relationship',
				'return_format' => 'object',
				'post_type' => array (
					0 => 'post',
				),
				'taxonomy' => array (
					0 => 'all',
				),
				'filters' => array (
					0 => 'search',
				),
				'result_elements' => array (
					0 => 'featured_image',
					1 => 'post_type',
					2 => 'post_title',
				),
				'max' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'employee',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

function register_scripts() {
	wp_enqueue_script( 'functions-js', get_theme_file_uri( '/assets/js/functions.js' ), array( 'jquery' ), '1.0', true );


	wp_localize_script( 'functions-js', 'ajax_object',
		array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

	wp_enqueue_script( 'functions-js' );

}

add_action( 'wp_enqueue_scripts', 'register_scripts' );

add_action('wp_ajax_do_action', 'display_posts');
add_action('wp_ajax_nopriv_do_action', 'display_posts');

function display_posts(){
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		$posts = get_posts('post_type=employee&order_by=title&order=ASC&posts_per_page=-1');
        $out= '';
		foreach($posts as $p) {
		    $out .='<div class="employee__post">';
                $image = get_field('image', $p->ID);
                if($image) {
                    $out .= wp_get_attachment_image( $image, 'medium' );
                }
                $out .= '<p>'.get_field('name', $p->ID).'</p>';
			$out .='</div>';
        }

		wp_send_json( array(
				'posts' => $out
			)
		);
	}
}


?>