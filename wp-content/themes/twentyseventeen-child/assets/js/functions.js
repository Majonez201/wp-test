(function ($) {
    var mapGenerator = (function () {
        var _init = function () {
            if ($('.marker').length) {

                var ZOOM_INDEX = 17;
                var description = $('.marker').data('description');
                var location = {lat: $('.marker').data('lat'), lng: $('.marker').data('lng')};
                _initMap(ZOOM_INDEX, location, description, 'map');
            }
        };

        var _initMap = function (_zoom, _location, _description, _id) {
            var map = new google.maps.Map(document.getElementById(_id), {
                zoom: _zoom,
                center: _location
            });

            var infowindow = new google.maps.InfoWindow({
                    content: _description
            });

            var marker = new google.maps.Marker({
                    position: _location,
                    map: map
            });

            marker.addListener('click', function() {
                infowindow.open( map, marker );
            });
        };

        return {
            init: _init
        }
    })();

    var employeesGenerator = (function() {
        var _init = function() {
            var data = {action: 'do_action'};
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function (response) {
                    $('.ajax-loader').hide();
                    console.log(response);
                    $('.employees').html(function () {
                        return response.posts;
                    });
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('.ajax-loader').hide();
                    $('.employees').html(function () {
                        return '<div style="color: red;">Error!!!</div>';
                    });
                }
            });
        };
        return {
            init: _init
        }
    }());

    $(document).ready(function(){
        mapGenerator.init();
        employeesGenerator.init();
    });

}(jQuery));
