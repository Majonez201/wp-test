<?php
/*
* Template Name: Contact
*/

get_header();
the_post();
?>
	<div class="wrap contact">
		<div class="article contact__row">
			<div class="contact__text">
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</div>
		</div>
		<div class="contact__row">
			<div class="contact__address">
				<?php
				$logo = get_field('logo');
				if( $logo ) {
					?>
					<div class="contact__logo">
						<?php echo wp_get_attachment_image( $logo, 'full' ); ?>
					</div>
					<?php
				}
				?>
                <?php echo get_field('address'); ?>
			</div>
			<div id="map" class="contact__map">
				<?php
				$location = get_field('map');
				if( !empty($location) ):
					?>
					<div class="marker" data-description="<?php echo get_field('address');?>" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
				<?php endif ?>
			</div>
		</div>
        <div class="employees">
            <div class="ajax-loader"><img src="/wp-content/uploads/2017/11/loader.gif" alt="loader"/></div>
        </div>
	</div>
<?php
get_footer();
?>